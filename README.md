# NEST MicroServices Boilerplate

## Description

NEST Services Boilerplate repository for creating robust services for Microservices Platform.

## Installation

```bash
$ npm login
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```
