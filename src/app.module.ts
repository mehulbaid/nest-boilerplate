import { MasterModule } from './master/master.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ServiceDefinition, ServiceReg } from '@mehulbaid/abstract-template';

@Module({
  imports: [
    MasterModule,
    TypeOrmModule.forRoot({
      "type": "mysql",
      "host": "localhost",
      "port": 3306,
      "username": "root",
      "password": "root",
      "database": "dummyDashboard",
      "entities": [ServiceDefinition, ServiceReg],
      "synchronize": true,
      "autoLoadEntities": true
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
