import { Controller } from '@nestjs/common';
import { serviceConf } from './master.config';
import { MasterService } from './master.service';

import { AbstractController } from '@mehulbaid/abstract-template';

@Controller((serviceConf.name).toLowerCase())
export class MasterController extends AbstractController {
  constructor(private masterService: MasterService) {
    super(masterService);
  }
}
