import { Injectable } from '@nestjs/common';
import { AbstractService, ServiceDefinition, ServiceReg } from '@mehulbaid/abstract-template';
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm';
import { serviceConf } from './master.config';
import { launch, Page } from 'puppeteer';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class MasterService extends AbstractService {

  public serviceObj = new BehaviorSubject<any>(null);
    
  constructor(
    @InjectRepository(ServiceDefinition) private serviceDef: Repository<ServiceDefinition>,
    @InjectRepository(ServiceReg) private serviceReg: Repository<ServiceReg>
  ) {
    super(serviceConf, serviceDef, serviceReg);
  }

  getCustomStatistics(): Promise<void> {
    return;
  }

  async getUpdatedRowsCount(days: number): Promise<number> {
    return days;
  }

  getLogStream(): Promise<void> {
    return;
  }

  reportCompletion(): Promise<void> {
    return;
  }

  reportCrash(): Promise<void> {
    return;
  }

  reportError(): Promise<void> {
    return;
  }

  reportSuccess(): Promise<void> {
    return;
  }

  reportWarning(): Promise<void> {
    return;
  }

  runScheduledTask(): Promise<void> {
    return;
  }

  startServiceImpl(): Promise<void> {
    return;
  }

  async performTask(): Promise<any> {
    const browser = await launch({ args: ['--no-sandbox', '--disable-setuid-sandbox'] });
    this.serviceObj.next(browser);
    const page = await browser.newPage();
    await page.goto('https://www.google.com', { waitUntil: 'networkidle2' });
    await page.pdf({ format: 'A4', path: __dirname + 'hn.pdf' })
  }

  async restartTask(): Promise<any> {
    return;
  }

  async stopPerformTask(): Promise<any> {
    let instance: Page;
    this.serviceObj.subscribe(res => instance = res);
    instance.close();
    return;
  }

}
