import { IServiceConf } from '@mehulbaid/abstract-template';

export const serviceConf: IServiceConf = {
  name: 'Google',
  category: 'Scraping',
  hostConfig: {
    hostname: 'localhost',
    port: 3001
  },
  scheduleInfo: {}
}