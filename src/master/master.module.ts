import { MasterController } from './master.controller';
import { MasterService } from './master.service';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServiceDefinition, ServiceReg } from '@mehulbaid/abstract-template';


@Module({
    imports: [
        TypeOrmModule.forFeature([
            ServiceDefinition,
            ServiceReg
        ])
    ],
    controllers: [
        MasterController
    ],
    providers: [
        MasterService
    ],
})
export class MasterModule {}
