import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

import * as compression from 'compression';
import * as helmet from 'helmet';

// Swagger
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });

  app.use(compression());
  app.use(helmet());

  const swaggerOptions = new DocumentBuilder()
    .setTitle('Services Boilerplate API')
    .setDescription('Services Boilerplate API Document for testing of service library endpoint')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, swaggerOptions);

  SwaggerModule.setup('apidoc', app, document, {
    customSiteTitle: 'Micro-Service SwaggerUi'
  });

  await app.listen(5000);
}
bootstrap();
